const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RecipeSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  ingredients: [{
    name: {type: String, required: true},
    amount: {type: String, required: true}
  }],
  image: {
    type: String,
    required: true
  },
  ratings: [{
    user: {type: Schema.ObjectId, ref: 'User', required: true},
    rating: {type: Number, required: true}
  }],
  published: {
    type: Boolean,
    required: true,
    default: false
  }
});

const Recipe = mongoose.model('Recipe', RecipeSchema);

module.exports = Recipe;
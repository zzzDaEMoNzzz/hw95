const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Recipe = require('./models/Recipe');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [admin, user] = await User.create(
    {
      username: 'test_fjngobx_admin@tfbnw.net',
      displayName: 'Test Admin',
      role: 'admin',
      facebookId: 110156853559321,
      password: nanoid(),
      token: nanoid()
    },
    {
      username: 'test_cvedosl_user@tfbnw.net',
      displayName: 'Test User',
      role: 'user',
      facebookId: 110772656830447,
      password: nanoid(),
      token: nanoid()
    }
  );

  await Recipe.create(
    {
      name: 'Mojito',
      description: '1. Muddle sugar and lime wedges together in a glass. ' +
        'Press down on 2 wedges of lime and 2 teaspoons of caster sugar in a tankard or jar using a large spoon or pestle to extract flavour and aroma.' +
        '\n2. Muddle mint. ' +
        'Pick 12 leaves from a sprig of mint and place in the glass. Press down gently on the mint, together with the sugar and lime.' +
        '\n3. Add ice to a glass. ' +
        'Add crushed ice so the glass is ¾ full.' +
        '\n4. Add Captain Morgan White Rum® and soda. ' +
        'Pour in Captain Morgan® White Rum and a dash of soda water.' +
        '\n5. Stir with a spoon. ' +
        'Stir the mixture thoroughly using a bar spoon until well combined.' +
        '\n6. Add ice and a sprig of mint. ' +
        'Top up with more crushed ice and garnish with a sprig of mint.',
      image: '_mojito.jpg',
      ingredients: [
        {name: 'Captain Morgan® White Rum', amount: '50ml.'},
        {name: 'Soda Water', amount: '1dash(es)'},
        {name: 'Caster Sugar', amount: '2tsp'},
        {name: 'Lime Wedge', amount: '2wedge(s)'},
        {name: 'Mint Sprig', amount: '1sprig(s)'}
      ],
      ratings: [],
      published: false,
      user: user._id
    },
    {
      name: 'Tequila Pina Colada',
      description: '1. Pour Don Julio Blanco Tequila, coconut cream and pineapple juice into a blender. ' +
        'Using a jigger, measure 50ml Don Julio Blanco Tequila, 50ml coconut cream and 110ml pineapple juice into a blender.' +
        '\n2. Blend together. ' +
        'Blend the ingredients together until smooth.' +
        '\n3. Half-fill a glass with ice. ' +
        'Half-fill a Snifter glass with ice cubes.' +
        '\n4. Pour the mixture into the glass. ' +
        'Pour the mixture from the blender into the glass.' +
        '\n5. Garnish with a slice of pineapple. ' +
        'Cut a slice of pineapple with a sharp knife on a cutting board and slot over the side of the glass to garnish.',
      image: '_tequila.jpg',
      ingredients: [
        {name: 'Don Julio® Blanco Tequila', amount: '50ml.'},
        {name: 'Coconut Cream', amount: '50ml.'},
        {name: 'Pineapple Juice', amount: '110ml.'},
        {name: 'Pineapple', amount: '3slice(s)'}
      ],
      ratings: [],
      published: false,
      user: admin._id
    },
    {
      name: 'Margarita',
      description: '1. Fill a shaker with ice. ' +
        'Fill a cocktail shaker with cubed ice.' +
        '\n2. Add Orange Liqueur, Don Julio Blanco Tequila and lime juice to the shaker. ' +
        'Use a jigger to measure 35ml Orange Liqueur, 50ml Don Julio Tequila and 25ml lime juice to the shaker..' +
        '\n3. Shake until cold. ' +
        'Shake until chilled.' +
        '\n4. Strain into a short glass. ' +
        'Using a cocktail strainer, strain into a short glass.',
      image: '_margarita.jpg',
      ingredients: [
        {name: 'Don Julio® Blanco Tequila', amount: '50ml.'},
        {name: 'Orange Liqueur', amount: '35ml.'},
        {name: 'Lime Juice', amount: '25ml.'}
      ],
      ratings: [],
      published: true,
      user: user._id
    }
  );

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});
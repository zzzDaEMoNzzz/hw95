const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: 'mongodb://localhost/recipes',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '1314214352064533',
    appSecret: 'e7991e398501fc5e0a74fe94576486be' // insecure!
  }
};

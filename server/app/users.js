const express = require('express');
const axios = require('axios');
const nanoid = require('nanoid');
const auth = require('../middleware/auth');
const config = require('../config');
const User = require('../models/User');

const router = express.Router();

router.get('/sessions', auth(), (req, res) => {
  res.send({message: 'Ok'});
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Logged out'};

  if (!token) {
    return res.send(success);
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.send(success);
  }

  user.generateToken();
  await user.save();

  return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    const responseData = response.data.data;

    if (response.error) {
      return res.status(500).send({error: 'Token incorrect'});
    }

    if (responseData.user_id !== req.body.id) {
      return res.status(500).send({error: 'User is wrong'});
    }

    let user = await User.findOne({facebookId: req.body.id});

    if (!user) {
      user = new User({
        username: req.body.email || req.body.id,
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name,
        avatar: req.body.picture.data.url
      });
    }

    user.generateToken();

    await user.save();

    res.send(user);
  } catch (e) {
    return res.status(500).send({error: 'Something went wrong'});
  }
});

module.exports = router;
const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const upload = require('../middleware/upload');
const Recipe = require('../models/Recipe');

const router = express.Router();

router.get('/', auth(false), async (req, res) => {
  const getAverageRating = ratings => {
    const ratingsSum = ratings.reduce((num, item) => {
      return num + item.rating;
    }, 0);

    return (ratingsSum === 0 ? ratingsSum : ratingsSum / ratings.length).toFixed(1);
  };

  try {
    const isAdmin = req.user && req.user.role === 'admin';

    let criteria = {};

    if (req.query.user) {
      criteria = {user: req.query.user};
    } else {
      if (!isAdmin) {
        criteria.published = true;
      }
    }

    let recipes = await Recipe.find(criteria).lean();

    recipes = recipes.map(recipe => {
      return {
        ...recipe,
        ratings: getAverageRating(recipe.ratings),
        description: undefined,
        ingredients: undefined
      };
    });

    res.send(recipes);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get('/:id', auth(false), async (req, res) => {
  try {
    const recipe = await Recipe.findById(req.params.id).lean();

    if (!recipe) {
      return res.sendStatus(404);
    }

    res.send(recipe);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.delete('/:id', [auth(), permit('admin')], async (req, res) => {
  try {
    const recipe = await Recipe.findById(req.params.id);

    if (!recipe) {
      return res.sendStatus(404);
    }

    const result = await Recipe.deleteOne({_id: recipe._id});

    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.post('/', [auth(), upload.single('image')], async (req, res) => {
  try {
    const recipeData = {
      name: req.body.name,
      description: req.body.description,
      ingredients: JSON.parse(req.body.ingredients),
      user: req.user._id
    };

    if (req.file) {
      recipeData.image = req.file.filename;
    }

    const recipe = new Recipe(recipeData);

    const result = await recipe.save();

    res.send(result);
  } catch (e) {
    res.status(400).send({error});
  }
});

router.post('/:id/add_rating', auth(), async (req, res) => {
  try {
    const recipe = await Recipe.findById(req.params.id);

    if (!recipe) {
      return res.sendStatus(404);
    }

    if (!req.body.rating) {
      return res.sendStatus(400);
    }

    const userRatingId = recipe.ratings.findIndex(item => item.user.equals(req.user._id));

    if (userRatingId === -1) {
      recipe.ratings.push({
        user: req.user._id,
        rating: req.body.rating
      });
    } else {
      recipe.ratings[userRatingId] = {
        user: req.user._id,
        rating: req.body.rating
      };
    }

    await recipe.save();

    res.send(recipe);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/:id/toggle_published', [auth(), permit('admin')], async (req, res) => {
  try {
    const recipe = await Recipe.findById(req.params.id);

    if (!recipe) {
      return res.sendStatus(404);
    }

    recipe.published = !recipe.published;

    await recipe.save();

    res.send(recipe);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
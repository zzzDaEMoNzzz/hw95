import React, {Component} from 'react';
import {connect} from "react-redux";
import CocktailsList from "../../components/CocktailsList/CocktailsList";
import {cleanCocktailsList, getCocktails, toggleCocktailPublished} from "../../store/actions/cocktailsActions";

class Cocktails extends Component {
  componentDidMount() {
    this.props.getCocktails();
  }

  componentWillUnmount() {
    this.props.cleanCocktailsList();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.user !== this.props.user) {
      this.props.getCocktails();
    }
  }

  togglePublished = (event, id) => {
    event.preventDefault();

    this.props.togglePublished(id, this.props.getCocktails);
  };

  render() {
    return (
      <CocktailsList
        data={this.props.cocktails}
        user={this.props.user}
        togglePublished={this.togglePublished}
      />
    );
  }
}

const mapStateToProps = state => ({
  cocktails: state.cocktails.cocktails,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getCocktails: () => dispatch(getCocktails()),
  cleanCocktailsList: () => dispatch(cleanCocktailsList()),
  togglePublished: (id, callback) => dispatch(toggleCocktailPublished(id, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);
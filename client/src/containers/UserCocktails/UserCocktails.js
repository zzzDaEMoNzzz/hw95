import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import CocktailsList from "../../components/CocktailsList/CocktailsList";
import {cleanCocktailsList, getCocktails, toggleCocktailPublished} from "../../store/actions/cocktailsActions";

class UserCocktails extends Component {
  componentDidMount() {
    this.props.getCocktails(this.props.user._id);
  }

  componentWillUnmount() {
    this.props.cleanCocktailsList();
  }

  togglePublished = (event, id) => {
    event.preventDefault();

    this.props.togglePublished(id, () => this.props.getCocktails(this.props.user._id));
  };

  render() {
    return (
      <Fragment>
        <h2>Your publications</h2>
        {this.props.cocktails && this.props.cocktails.length === 0 && (
          <p>You haven't posted anything yet.</p>
        )}
        <CocktailsList
          data={this.props.cocktails}
          user={this.props.user}
          togglePublished={this.togglePublished}
          showStatus={true}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cocktails: state.cocktails.cocktails,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getCocktails: userId => dispatch(getCocktails(userId)),
  cleanCocktailsList: () => dispatch(cleanCocktailsList()),
  togglePublished: (id, callback) => dispatch(toggleCocktailPublished(id, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserCocktails);
import React, {Component} from 'react';
import {connect} from "react-redux";
import Rating from 'react-rating';
import {apiURL} from "../../constants";
import './CocktailInfo.css';

import emptyStar from "../../assetts/images/star-empty.png";
import fullStar from "../../assetts/images/star-full.png";

import {
  addCocktailRating,
  cleanCocktailInfo,
  deleteCocktail,
  getCocktailInfo
} from "../../store/actions/cocktailsActions";

class CocktailInfo extends Component {
  componentDidMount() {
    this.props.getCocktailInfo(this.props.match.params.id);
  }

  componentWillUnmount() {
    this.props.cleanCocktailInfo();
  }

  getAverageRating = ratings => {
    const ratingsSum = ratings.reduce((num, item) => {
      return num + item.rating;
    }, 0);

    return (ratingsSum === 0 ? ratingsSum : ratingsSum / ratings.length).toFixed(1);
  };

  deleteCocktail = id => {
    if (window.confirm('Are you sure you want to delete this?')) {
      this.props.deleteCocktail(id);
    }
  };

  render() {
    if (!this.props.cocktailInfo){
      return <h2>Loading...</h2>;
    }

    if (!this.props.cocktailInfo.published) {
      if (this.props.user.role !== 'admin' && (!this.props.user || this.props.user._id !== this.props.cocktailInfo.user)) {
        this.props.history.replace('/');
      }
    }

    const rating = this.getAverageRating(this.props.cocktailInfo.ratings);

    let userRating = 0;

    if (this.props.user) {
      const userHaveRating = this.props.cocktailInfo.ratings.find(item => item.user === this.props.user._id);

      userRating = userHaveRating  ? userHaveRating.rating : 0;
    }

    return (
      <div className="CocktailInfo">
        <img
          className="CocktailInfo-image"
          src={`${apiURL}/uploads/${this.props.cocktailInfo.image}`}
          alt=""
        />
        <div className="CocktailInfo-rating">
          <div>
            <Rating
              emptySymbol={<img src={emptyStar} className="CocktailsInfo-ratingIcon" alt=""/>}
              fullSymbol={<img src={fullStar} className="CocktailsInfo-ratingIcon" alt=""/>}
              initialRating={this.props.user ? userRating : rating}
              onChange={value => this.props.addCocktailRating(this.props.cocktailInfo._id, value)}
              readonly={!this.props.user || !this.props.cocktailInfo.published}
            />
          </div>
          <div>
            <span>- {Number(rating)}</span>
            <span className="CocktailInfo-ratingVotes">({this.props.cocktailInfo.ratings.length} votes)</span>
          </div>
        </div>
        <h2>{this.props.cocktailInfo.name}</h2>
        <h4>Ingredients:</h4>
        <ul>
          {this.props.cocktailInfo.ingredients.map(ingredient => (
            <li key={`${ingredient.name}_${ingredient.amount}`}>
              {ingredient.name} - {ingredient.amount}
            </li>
          ))}
        </ul>
        <h4>Recipe:</h4>
        <p>{this.props.cocktailInfo.description}</p>
        {this.props.user && this.props.user.role === 'admin' && (
          <button
            className="CocktailInfo-deleteBtn"
            onClick={() => this.deleteCocktail(this.props.cocktailInfo._id)}
          >
            Delete
          </button>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  cocktailInfo: state.cocktails.cocktailInfo
});

const mapDispatchToProps = dispatch => ({
  getCocktailInfo: id => dispatch(getCocktailInfo(id)),
  cleanCocktailInfo: () => dispatch(cleanCocktailInfo()),
  addCocktailRating: (id, rating) => dispatch(addCocktailRating(id, rating)),
  deleteCocktail: id => dispatch(deleteCocktail(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(CocktailInfo);
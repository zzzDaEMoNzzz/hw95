import React, {Component, Fragment, createRef} from 'react';
import {connect} from "react-redux";
import './AddCocktail.css';
import {addCocktail} from "../../store/actions/cocktailsActions";

class AddCocktail extends Component {
  state = {
    name: '',
    ingredients: [
      {name: '', amount: ''}
    ],
    description: '',
    image: null
  };

  imageInput = createRef();

  onIngredientChange = (event, index, key) => {
    const ingredients = [...this.state.ingredients];
    ingredients[index][key] = event.target.value;

    this.setState({ingredients});
  };

  addIngredient = () => {
    const ingredients = [
      ...this.state.ingredients,
      {name: '', amount: ''}
    ];

    this.setState({ingredients});
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({image: event.target.files[0] || null});
  };

  removeIngredient = index => {
    const ingredients = [...this.state.ingredients];
    ingredients.splice(index, 1);

    this.setState({ingredients});
  };

  onSubmit = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      if (key === 'ingredients') {
        formData.append(key, JSON.stringify(this.state[key]));
      } else {
        formData.append(key, this.state[key]);
      }
    });

    this.props.addCocktail(formData);
  };

  render() {
    return (
      <Fragment>
        <h2>Add new cocktail</h2>
        <form className="AddCocktail" onSubmit={this.onSubmit}>
          <div className="AddCocktail-row">
            <label>Name</label>
            <input
              type="text"
              name="name"
              value={this.state.name}
              onChange={this.inputChangeHandler}
              required
            />
          </div>
          <div className="AddCocktail-row">
            <label>Ingredients</label>
            <div className="AddCocktail-ingredients">
              {this.state.ingredients.map((ingredient, index) => (
                <div key={`ingredient_${index}`}>
                  <input
                    type="text"
                    placeholder="Ingredient name"
                    value={this.state.ingredients[index].name}
                    onChange={event => this.onIngredientChange(event, index, 'name')}
                    required
                  />
                  <input
                    type="text"
                    placeholder="Amount"
                    value={this.state.ingredients[index].amount}
                    onChange={event => this.onIngredientChange(event, index, 'amount')}
                    required
                  />
                  {index > 0 && (
                    <button
                      onClick={() => this.removeIngredient(index)}
                      type="button"
                      className="AddCocktail-removeIngredientBtn"
                    >
                      X
                    </button>
                  )}
                </div>
              ))}
              <button
                onClick={this.addIngredient}
                type="button"
                className="AddCocktail-addIngredientBtn"
              >
                Add ingredient
              </button>
            </div>
          </div>
          <div className="AddCocktail-row">
            <label>Recipe</label>
            <textarea
              name="description"
              rows="5"
              value={this.state.description}
              onChange={this.inputChangeHandler}
              required
            />
          </div>
          <div className="AddCocktail-row">
            <label>Image</label>
            <div className="AddCocktail-image">
              <input
                type="file"
                accept="image/*"
                ref={this.imageInput}
                onChange={this.fileChangeHandler}
                required
              />
              <button
                type="button"
                onClick={() => this.imageInput.current.click()}
              >
                Choose file
              </button>
              <span>{(this.state.image && this.state.image.name) || 'No file chosen'}</span>
            </div>
          </div>
          <div className="AddCocktail-row">
            <button className="AddCocktail-submitBtn">Add cocktail</button>
          </div>
        </form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addCocktail: cocktailData => dispatch(addCocktail(cocktailData))
});

export default connect(null, mapDispatchToProps)(AddCocktail);
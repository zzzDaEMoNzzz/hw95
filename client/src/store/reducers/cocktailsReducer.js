import {
  CLEAN_COCKTAIL_INFO,
  CLEAN_COCKTAILS_LIST,
  GET_COCKTAIL_INFO_SUCCESS,
  GET_COCKTAILS_SUCCESS
} from "../actions/cocktailsActions";

const initialState = {
  cocktails: [],
  cocktailInfo: null
};

const cocktailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_COCKTAILS_SUCCESS:
      return {...state, cocktails: action.cocktails};
    case CLEAN_COCKTAILS_LIST:
      return {...state, cocktails: []};
    case GET_COCKTAIL_INFO_SUCCESS:
      return {...state, cocktailInfo: action.cocktailInfo};
    case CLEAN_COCKTAIL_INFO:
      return {...state, cocktailInfo: null};
    default:
      return state;
  }
};

export default cocktailsReducer;
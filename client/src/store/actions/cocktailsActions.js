import axios from '../../axios-api';
import {push, replace} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const GET_COCKTAILS_SUCCESS = 'GET_COCKTAILS_SUCCESS';
const getCocktailsSuccess = cocktails => ({type: GET_COCKTAILS_SUCCESS, cocktails});

export const getCocktails = userId => {
  let url = '/recipes';

  if (userId) {
    url += `/?user=${userId}`;
  }

  return dispatch => {
    axios.get(url).then(
      response => dispatch(getCocktailsSuccess(response.data)),
      () => {
        NotificationManager.error('An error has occurred when loading data');
      }
    )
  };
};

export const CLEAN_COCKTAILS_LIST = 'CLEAN_COCKTAILS_LIST';
export const cleanCocktailsList = () => ({type: CLEAN_COCKTAILS_LIST});

export const addCocktail = cocktailData => {
  return dispatch => {
    axios.post('/recipes', cocktailData).then(
      () => {
        NotificationManager.success('Cocktail successfully added! \nAnd will be published after being moderated.');
        dispatch(push('/'));
      },
      () => {
        NotificationManager.error('Something went wrong');
      }
    )
  };
};

export const GET_COCKTAIL_INFO_SUCCESS = 'GET_COCKTAIL_INFO_SUCCESS';
const getCocktailInfoSuccess = cocktailInfo => ({type: GET_COCKTAIL_INFO_SUCCESS, cocktailInfo});

export const getCocktailInfo = id => {
  return dispatch => {
    axios.get(`/recipes/${id}`).then(
      response => dispatch(getCocktailInfoSuccess(response.data)),
      () => {
        NotificationManager.error('An error has occurred when loading data');
      }
    )
  };
};

export const CLEAN_COCKTAIL_INFO = 'CLEAN_COCKTAIL_INFO';
export const cleanCocktailInfo = () => ({type: CLEAN_COCKTAIL_INFO});

export const deleteCocktail = id => {
  return dispatch => {
    axios.delete(`/recipes/${id}`).then(
      () => {
        NotificationManager.success('Cocktail successfully deleted');
        dispatch(replace('/'));
      }
    )
  };
};

export const addCocktailRating = (id, rating) => {
  return dispatch => {
    axios.post(`/recipes/${id}/add_rating`, {rating}).then(
      response => dispatch(getCocktailInfoSuccess(response.data)),
      () => {
        NotificationManager.error('Something went wrong');
      }
    );
  };
};

export const toggleCocktailPublished = (id, callback) => {
  return dispatch => {
    axios.post(`/recipes/${id}/toggle_published`).then(
      () => callback()
    );
  };
};
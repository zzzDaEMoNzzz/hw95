import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});

export const facebookLogin = userData => {
  return dispatch => {
    return axios.post('/users/facebookLogin', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        NotificationManager.success('Logged in via Facebook');
        dispatch(push('/'));
      },
      () => {
        NotificationManager.error('Login via Facebook failed');
      }
    )
  };
};

export const LOGOUT_USER = 'LOGOUT_USER';

export const logoutUser = () => {
  return dispatch => {
    return axios.delete('/users/sessions').then(
      () => {
        dispatch({type: LOGOUT_USER});
        NotificationManager.success('Logged out!');
      },
      () => {
        NotificationManager.error('Could not logout');
      }
    );
  };
};

export const checkAuth = () => {
  return dispatch => {
    axios.get('/users/sessions').then(
      null,
      () => dispatch({type: LOGOUT_USER})
    );
  };
};
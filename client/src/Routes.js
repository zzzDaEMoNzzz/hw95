import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Cocktails from "./containers/Cocktails/Cocktails";
import AddCocktail from "./containers/AddCocktail/AddCocktail";
import CocktailInfo from "./containers/CocktailInfo/CocktailInfo";
import UserCocktails from "./containers/UserCocktails/UserCocktails";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props}/> : <Redirect to="/"/>;
};

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={Cocktails}/>
      <Route path="/cocktails" exact component={Cocktails}/>
      <ProtectedRoute
        isAllowed={!!user}
        path="/cocktails/add"
        exact
        component={AddCocktail}
      />
      <ProtectedRoute
        isAllowed={!!user}
        path="/user_cocktails"
        exact
        component={UserCocktails}
      />
      <Route path="/cocktails/:id" exact component={CocktailInfo}/>
      <Route render={() => <h2>Page not found...</h2>}/>
    </Switch>
  );
};

export default Routes;

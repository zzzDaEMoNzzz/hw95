import React from 'react';
import Rating from 'react-rating';
import {Link} from "react-router-dom";
import {apiURL} from "../../constants";
import './CocktailsList.css';

import emptyStar from '../../assetts/images/star-empty.png';
import fullStar from '../../assetts/images/star-full.png';

const CocktailsList = ({data, user, showStatus = false, togglePublished}) => {
  const userIsAdmin = user && user.role === 'admin';

  const publishedStatus = (published, id) => {
    const classes = ['CocktailsList-itemUnpublished'];

    if (!published) {
      classes.push('unpublished');
    }

    return (
      <div
        className={classes.join(' ')}
        onClick={(userIsAdmin && togglePublished) ? event => togglePublished(event, id) : null}
      >
        {published ? 'Published' : 'Unpublished'}
      </div>
    );
  };

  return (
    <div className="CocktailsList">
      {data.map(cocktail => (
        <Link to={`/cocktails/${cocktail._id}`} key={cocktail._id} className="CocktailsList-item">
          <div
            className="CocktailsList-itemImage"
            style={{
              backgroundImage: `url(${apiURL}/uploads/${cocktail.image})`
            }}
          />
          <div className="CocktailsList-itemBody">
            <div className="CocktailsList-itemHeader">
              <Rating
                emptySymbol={<img src={emptyStar} className="CocktailsList-ratingIcon" alt=""/>}
                fullSymbol={<img src={fullStar} className="CocktailsList-ratingIcon" alt=""/>}
                initialRating={cocktail.ratings}
                readonly
              />
              {(userIsAdmin || showStatus) && publishedStatus(cocktail.published, cocktail._id)}
            </div>
            <div className="CocktailsList-itemName">{cocktail.name}</div>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default CocktailsList;

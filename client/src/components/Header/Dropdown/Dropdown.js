import React, {Component} from 'react';
import {Link, NavLink} from "react-router-dom";
import './Dropdown.css';

class Dropdown extends Component {
  state = {
    showMenu: false
  };

  toggleMenu = () => {
    if (!this.state.showMenu) {
      this.setState({showMenu: true});
      document.addEventListener('click', this.toggleMenu);
    } else {
      this.setState({showMenu: false});
      document.removeEventListener('click', this.toggleMenu);
    }
  };

  render() {
    return (
      <div className="Dropdown">
        <button
          onClick={this.toggleMenu}
          className={"Dropdown-btn " + (this.state.showMenu ? "arrowUp" : "arrowDown")}
        >
          <img src={this.props.avatar} alt=""/>
          <span>{this.props.displayName}</span>
        </button>
        <div
          className="Dropdown-menu"
          style={{display: this.state.showMenu ? 'block' : 'none'}}
        >
          <nav>
            <NavLink to="/cocktails/add" exact>Add cocktail</NavLink>
            <NavLink to="/user_cocktails" exact>My cocktails</NavLink>
            <Link to="/" onClick={this.props.logoutUser}>Logout</Link>
          </nav>
        </div>
      </div>
    );
  }
}

export default Dropdown;
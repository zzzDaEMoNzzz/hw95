import React from 'react';
import {NavLink, withRouter} from "react-router-dom";
import './Header.css';
import FacebookLogin from "../FacebookLogin/FacebookLogin";
import Dropdown from "./Dropdown/Dropdown";

import noAvatarImg from '../../assetts/images/noAvatar.png';

const Header = props => {
  const anonMenu = (
    <FacebookLogin/>
  );

  const userMenu = (
    <Dropdown
      displayName={props.user && props.user.displayName}
      avatar={(props.user && props.user.avatar) ? props.user.avatar : noAvatarImg}
      logoutUser={props.logoutUser}
    />
  );

  const isCocktailsPage = () => {
    const path = props.location.pathname;
    return path === '/' || path === '/cocktails' || path === '/cocktails/';
  };

  return (
    <div className="Header">
      <NavLink to="/" isActive={isCocktailsPage}>Cocktails</NavLink>
      {props.user ? userMenu : anonMenu}
    </div>
  );
};

export default withRouter(Header);

## Test Users
| role  | login                          | password  |
| :---- | :----------------------------- | :-------- |
| admin | `test_fjngobx_admin@tfbnw.net` | `qw123e`  |
| user  | `test_cvedosl_user@tfbnw.net`  | `q12w3e`  |